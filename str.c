void
str_copy (void *to, void *from, int maxlen)
{
  int i;
  char *p, *q;

  if (!maxlen)
    return;
  p = to;
  q = from;
  for (i = 0; i < maxlen - 1 && q[i]; i++)
    p[i] = q[i];
  p[i] = 0;
}
