unsigned int *rsdp, *xsdt, *rsdt, *facp1, *facp2;

static void *
rsdpsearch (unsigned long vec, unsigned int count)
{
  char k[] = "RSD PTR ";
  unsigned int *p, *q;

  p = (void *)(vec << 4);
  q = (void *)k;
  while (count--)
    {
      if (p[0] == q[0] && p[1] == q[1])
	return p;
      p += 4;
    }
  return 0;
}

static void
find_tables (void)
{
  unsigned short *p40e = (void *)0x40e;
  int i, len;

  rsdp = xsdt = rsdt = facp1 = facp2 = 0;

  /* RSDP */
  rsdp = rsdpsearch (*p40e, 0x40);
  if (!rsdp)
    rsdp = rsdpsearch (0xe000, 0x2000);
  if (!rsdp)
    {
      print ("rsdp not found\n");
      for (;;);
    }
  print ("rsdp at %x\n", rsdp);

  /* RSDT */
  rsdt = (void *)rsdp[4];
  print ("rsdt at %x\n", rsdt);

  /* XSDT */
  if ((rsdp[3] >> 24) == 2)
    xsdt = (void *)rsdp[6];
  if (xsdt)
    print ("xsdt at %x\n", xsdt);

  /* FACP in RSDT */
  len = rsdt[1] >> 2;
  for (i = 9; i < len; i++)
    {
      if (*(int *)rsdt[i] == *(int *)"FACP")
	{
	  facp1 = (void *)rsdt[i];
	  break;
	}
    }
  print ("facp1 at %x\n", facp1);

  /* FACP in XSDT */
  if (xsdt)
    {
      len = xsdt[1] >> 2;
      for (i = 9; i + 1 < len; i += 2)
	{
	  if (xsdt[i + 1] == 0 && *(int *)xsdt[i] == *(int *)"FACP")
	    {
	      facp2 = (void *)xsdt[i];
	      break;
	    }
	}
      print ("facp2 at %x\n", facp2);
    }
}

static unsigned char
sum4 (unsigned int v)
{
  unsigned char *p;

  p = (void *)&v;
  return p[0] + p[1] + p[2] + p[3];
}

static void
modify_facp_part (unsigned int *facp, unsigned long newfacs)
{
  unsigned char *checksum;

  checksum = ((unsigned char *)facp) + 9;
  if (facp[1] >= 140)
    {
      *checksum += sum4 (facp[33]) + sum4 (facp[34]);
      facp[33] = newfacs;
      facp[34] = 0;
      *checksum -= sum4 (facp[33]) + sum4 (facp[34]);
    }
  if (facp[1] >= 40)
    {
      *checksum += sum4 (facp[9]);
      facp[9] = newfacs;
      *checksum -= sum4 (facp[9]);
    }
}

void
modify_facp (unsigned long newfacs)
{
  if (facp1)
    modify_facp_part (facp1, newfacs);
  if (facp2)
    modify_facp_part (facp2, newfacs);
}

unsigned int
get_facs_addr (void)
{
  if (facp1)
    return facp1[9];
  if (facp2)
    return facp2[9];
  return 0;
}

void
acpi_prepare (void)
{
  find_tables ();
}
