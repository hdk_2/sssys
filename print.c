int
getch (void)
{
  int c;

 loop:
  asm volatile ("int $0x16; mov $0, %0; je 1f; inc %0; 1:"
		: "=a" (c) : "a" (0x100));
  if (!c)
    {
      hlt ();
      goto loop;
    }
  asm volatile ("int $0x16" : "=a" (c) : "a" (0));
  return c & 255;
}

void
putch (int c)
{
  asm volatile ("int $0x10" : : "a" (0x0e00 + c), "b" (7));
}

static void
printhex0 (char d)
{
  putch ("0123456789abcdef"[d & 0xf]);
}

static void
printhex1 (char d)
{
  printhex0 (d >> 4);
  printhex0 (d);
}

static void
printhex2 (short d)
{
  printhex1 (d >> 8);
  printhex1 (d);
}

void
printhex (int d)
{
  printhex2 (d >> 16);
  printhex2 (d);
}

void
print (char *fmt, ...)
{
  int *arg;
  int c;
  char *q;

  arg = (int *)&fmt;
  arg++;
  while ((c = *fmt++))
    {
      switch (c)
	{
	case '%':
	  switch (c = *fmt++)
	    {
	    case '%':
	      putch (c);
	      break;
	    case 'x':
	      printhex (*arg++);
	      break;
	    case 's':
	      q = (char *)*arg++;
	      while ((c = *q++))
		putch (c);
	      break;
	    case 'c':
	      putch (*arg++);
	      break;
	    default:
	      return;
	    }
	  break;
	case '\n':
	  putch ('\r');
	default:
	  putch (c);
	}
    }
}
