	MULTIBOOT_PAGE_ALIGN = (1 << 0)
	MULTIBOOT_MEMORY_INFO = (1 << 1)

	MULTIBOOT_HEADER_MAGIC = 0x1BADB002
	MULTIBOOT_HEADER_FLAGS = (MULTIBOOT_PAGE_ALIGN | MULTIBOOT_MEMORY_INFO)
	CHECKSUM = -(MULTIBOOT_HEADER_MAGIC + MULTIBOOT_HEADER_FLAGS)

	.text
	# 0x00100000
	# GRUB Multiboot Header
	.align  4
	.long   MULTIBOOT_HEADER_MAGIC
	.long   MULTIBOOT_HEADER_FLAGS
	.long   CHECKSUM

	.global	_start
_start:	cld
	lgdt	gdt
	ljmp	$0x8, $1f
1:	mov	$0x10, %eax
	mov	%eax, %ds
	mov	%eax, %es
	mov	%eax, %ss
	mov	$gdt, %esp
	mov	$idt, %edi
	push	$int08
	popw	0x08*8+0(%edi)
	popw	0x08*8+6(%edi)
	push	$int0d
	popw	0x0d*8+0(%edi)
	popw	0x0d*8+6(%edi)
	lidt	(%edi)
	push	%ebx
	call	_main
int08:	push	$msgerr
	call	print
	hlt
msgerr:	.string	"FATAL ERROR\n"

int0d:	pushfl
	push	%eax
	mov	8(%esp), %eax
	test	$2, %eax
	je	int08
	shr	$3, %eax
	and	$0xff, %al
	mov	%al, int0dn
	mov	$0x20, %eax
	mov	%eax, %ds
	mov	%eax, %es
	mov	%eax, %ss
	mov	%cr0, %eax
	dec	%eax
	lidt	idt+8
	ljmp	$0x18, $1f
	.code16
1:	mov	%eax, %cr0
	ljmpl	$0xffff, $1f-0xffff0
1:	xor	%ax, %ax
	mov	%ax, %ds
	mov	%ax, %es
	dec	%ax
	mov	%ax, %ss
	sub	$0xffff0, %esp
	pop	%eax
	popfl
	.byte	0xcd
int0dn:	.byte	0x90
	cli
	pushfl
	push	%eax
	mov	%cr0, %eax
	inc	%ax
	mov	%eax, %cr0
	ljmpl	$0x8, $1f
	.code32
1:	mov	$0x10, %eax
	mov	%eax, %ds
	mov	%eax, %es
	mov	%eax, %ss
	add	$0xffff0, %esp
	lidt	idt
	addl	$2, 12(%esp)
	pop	%eax
	popfl
	lea	4(%esp), %esp
	lret	$4

	.global	hlt
hlt:	mov	$0x20, %eax
	mov	%eax, %ds
	mov	%eax, %es
	mov	%eax, %ss
	mov	%cr0, %eax
	dec	%eax
	lidt	idt+8
	ljmp	$0x18, $1f
	.code16
1:	mov	%eax, %cr0
	ljmpl	$0xffff, $1f-0xffff0
1:	mov	$0xffff, %ax
	mov	%ax, %ds
	mov	%ax, %es
	mov	%ax, %ss
	sub	$0xffff0, %esp
	sti
	hlt
	cli
	mov	%cr0, %eax
	inc	%ax
	mov	%eax, %cr0
	ljmpl	$0x8, $1f
	.code32
1:	mov	$0x10, %eax
	mov	%eax, %ds
	mov	%eax, %es
	mov	%eax, %ss
	add	$0xffff0, %esp
	lidt	idt
	ret
	.global	jmpfar
jmpfar:	pop	%eax
	mov	$0x20, %eax
	mov	%eax, %ds
	mov	%eax, %es
	mov	%eax, %ss
	mov	%cr0, %eax
	dec	%eax
	lidt	idt+8
	ljmp	$0x18, $1f
	.code16
1:	mov	%eax, %cr0
	ljmpl	$0xffff, $1f-0xffff0
1:	mov	$0xffff, %ax
	mov	%ax, %ds
	mov	%ax, %es
	mov	%ax, %ss
	sub	$0xffff0, %esp
	lret
	.code32

	.align	8
	.space	1024
gdt:	.short	0xffff			# 0x00
	.long	gdt
	.short	0
	.quad	0x00cf9b000000ffff	# 0x08 code32
	.quad	0x00cf93000000ffff	# 0x10 data32
	.quad	0x008f9b000000ffff	# 0x18 code16
	.quad	0x008f93000000ffff	# 0x20 data16
idt:	.short	0x6f			# 0x00
	.long	idt
	.short	0
	.short	0x3ff			# 0x01
	.long	0
	.short	0
	.quad	0			# 0x02
	.quad	0			# 0x03
	.quad	0			# 0x04
	.quad	0			# 0x05
	.quad	0			# 0x06
	.quad	0			# 0x07
	.quad	0x00008e0000080000	# 0x08
	.quad	0			# 0x09
	.quad	0			# 0x0a
	.quad	0			# 0x0b
	.quad	0			# 0x0c
	.quad	0x00008e0000080000	# 0x0d
