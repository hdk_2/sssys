	.text
	.global	boot
boot:	push	%ebx
	push	%esi
	push	%edi
	mov	bootdrive, %dl
	mov	$0, %ah
	int	$0x13
	jc	err
	mov	$0x0201, %ax
	mov	$1, %cx
	mov	$0, %dh
	mov	$0x7c00, %bx
	int	$0x13
	jc	err
	push	$0x7c00
	call	jmpfar
err:	pop	%edi
	pop	%esi
	pop	%ebx
	ret
