char cmdline[1024];
unsigned char bootdrive;
int *mmap, mmaplen;

void
_main (void **grubdata)
{
  str_copy (cmdline, grubdata[4], 1024);
  bootdrive = (int)grubdata[3] >> 24;
  mmap = grubdata[12];
  mmaplen = (int)grubdata[11];
  acpi_prepare ();
  menu2 ();
}
