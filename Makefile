OBJS = head.o main.o print.o str.o map.o menu.o acpi.o reboot.o boot.o swap.o read.o
CFLAGS = -g -nostdinc -mno-red-zone -fno-stack-protector
LDFLAGS = -g -nostdlib -Wl,-Ttext,0x100000

.PHONY : all copy

all : a.out

a.out : $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS)

copy : all
	mcopy a.out a:/ && qemu -fda fdd
