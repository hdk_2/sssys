#define HEADERTEXT "SSSYS Ver.0.1"
#define FIRSTDATASIZE 16

extern int *mmap, mmaplen;
extern unsigned int wkrb1, wkpbse, wkpsp;
extern unsigned char wkrb2;
extern unsigned char wkr[], wkrend[], wkp[], wkp32[], wkpend[];
unsigned long all, halfaddr, alladdr, alllen;

#define VERIFYINFOMAX 1000
struct
{
  unsigned int addr, len;
} verifyinfo[VERIFYINFOMAX];

#define BLK 1024

void
dumpmap (void)
{
  int *d = mmap, len = mmaplen;

  while (len >= 4)
    {
      print ("size %x addr %x:%x length %x:%x type %x\n",
	     d[0], d[2], d[1], d[4], d[3], d[5]);
      len -= d[0] + 4;
      d = (int *)((int)d + d[0] + 4);
    }
}

static void
fillsub (unsigned a1, unsigned a0, unsigned b1, unsigned b0)
{
  unsigned *p, sum;
  int i;

  asm ("add %3, %1; adc %2, %0" : "+m" (b1), "+m" (b0) : "r" (a1), "r" (a0));
  if (a1)
    {
      print ("area above 4GB ignored\n");
      return;
    }
  if (b1)
    {
      print ("end address truncated to 4GB\n");
      b0 = ~0;
    }
  if (a0 < 0x110000)
    a0 = 0x110000;
  for (a0 = (a0 + BLK - 1) & ~(BLK - 1); a0 + (BLK - 1) <= b0; a0 += BLK)
    {
      if (!(a0 & 0xfffff))
	print ("\r%x", a0);
      p = (void *)a0;
      sum = 0;
      for (i = 1; i < (BLK / 4); i++)
	sum += p[i] = (a0 ^ ~i ^ 0x5555aaaa);
      p[0] = sum;
    }
}

static void
verifysub (int *v, unsigned a1, unsigned a0, unsigned b1, unsigned b0)
{
  unsigned *p, c, sum;
  int i, f;
  unsigned int total = 0;

  asm ("add %3, %1; adc %2, %0" : "+m" (b1), "+m" (b0) : "r" (a1), "r" (a0));
  if (a1)
    {
      print ("area above 4GB ignored\n");
      return;
    }
  if (b1)
    {
      print ("end address truncated to 4GB\n");
      b0 = ~0;
    }
  if (a0 < 0x110000)
    a0 = 0x110000;
  if (a0 >= b0)
    return;
  a0 = (a0 + BLK - 1) & ~(BLK - 1);
  c = a0;
  f = 1;
  for (; a0 + BLK - 1 <= b0; a0 += BLK)
    {
      if (!(a0 & 0xfffff))
	print ("%x\b\b\b\b\b\b\b\b", a0);
      p = (void *)a0;
      sum = 0;
      for (i = 1; i < (BLK / 4); i++)
	if (p[i] != (a0 ^ ~i ^ 0x5555aaaa))
	  break;
	else
	  sum += p[i];
      if (i == (BLK / 4) && p[0] != sum)
	i = 0;
      if (f && i < (BLK / 4))
	{
	  if (c != a0)
	    {
	      total += a0 - c;
	      verifyinfo[*v].addr = c;
	      verifyinfo[*v].len = a0 - c;
	      ++*v;
	      if (*v + 1 >= VERIFYINFOMAX)
		{
		  print ("VERIFYINFOMAX\n");
		  return;
		}
	      print ("%x-%xOK ", c, a0 - 1);
	      c = a0;
	    }
	  f = 0;
	}
      else if (!f && i == (BLK / 4))
	{
	  if (c != a0)
	    {
	      print ("%x-%xNG ", c, a0 - 1);
	      c = a0;
	    }
	  f = 1;
	}
    }
  if (f)
    {
      if (c != a0)
	{
	  total += a0 - c;
	  verifyinfo[*v].addr = c;
	  verifyinfo[*v].len = a0 - c;
	  ++*v;
	  if (*v + 1 >= VERIFYINFOMAX)
	    {
	      print ("VERIFYINFOMAX\n");
	      return;
	    }
	  print ("%x-%xOK", c, a0 - 1);
	}
    }
  else if (!f)
    {
      if (c != a0)
	{
	  print ("%x-%xNG", c, a0 - 1);
	}
    }
  print ("\n");
  print ("total %x\n", total);
}

void
filltest (void)
{
  int *d = mmap, len = mmaplen;

  while (len >= 4)
    {
      if (d[5] == 1)
	{
	  print ("size %x addr %x:%x length %x:%x type %x\n",
		 d[0], d[2], d[1], d[4], d[3], d[5]);
	  fillsub (d[2], d[1], d[4], d[3]);
	}
      len -= d[0] + 4;
      d = (int *)((int)d + d[0] + 4);
    }
  print (" done\n");
}

void
verify (void)
{
  int *d = mmap, len = mmaplen;
  int i = 0;

  while (len >= 4)
    {
      if (d[5] == 1)
	{
	  print ("size %x addr %x:%x length %x:%x type %x\n",
		 d[0], d[2], d[1], d[4], d[3], d[5]);
	  verifysub (&i, d[2], d[1], d[4], d[3]);
	}
      len -= d[0] + 4;
      d = (int *)((int)d + d[0] + 4);
    }
  verifyinfo[i].addr = 0;
  verifyinfo[i].len = 0;
}

void
splitaddr (void)
{
  int *d, len;
  unsigned long half;
  char m[5], *mm;

  all = 0;
  alladdr = 0;
  alllen = 0;
  d = mmap, len = mmaplen;
  while (len >= 4)
    {
      if (d[5] == 1)
	{
	  all += d[3];
	  if (alladdr < d[1] + d[3])
	    {
	      alladdr = d[1] + d[3];
	      alllen = d[3];
	    }
	}
      len -= d[0] + 4;
      d = (int *)((int)d + d[0] + 4);
    }
  print ("total memory %x\n", all);
  half = halfaddr = 0;
  d = mmap, len = mmaplen;
  while (len >= 4)
    {
      if (d[5] == 1)
	{
	  half += d[3];
	  if (half >= all / 2)
	    {
	      halfaddr = d[1] + d[3] - (half - all / 2);
	      break;
	    }
	}
      len -= d[0] + 4;
      d = (int *)((int)d + d[0] + 4);
    }
  m[0] = "0123456789"[(((all / 2 - 8192) / 1048576) / 1000) % 10];
  m[1] = "0123456789"[(((all / 2 - 8192) / 1048576) / 100) % 10];
  m[2] = "0123456789"[(((all / 2 - 8192) / 1048576) / 10) % 10];
  m[3] = "0123456789"[(((all / 2 - 8192) / 1048576) / 1) % 10];
  m[4] = '\0';
  for (mm = m; *mm == '0'; mm++);
  print ("1/2 %x (addr %x-) mem=%sM /MAXMEM=%s\n", all / 2, halfaddr, mm, mm);
}

static void
getnext (int **d, int *dlen, unsigned int *addr, unsigned int *len)
{
  *len = 0;
  while (*dlen >= 4)
    {
      if ((*d)[5] == 1)
	{
	  *addr = (*d)[1];
	  *len = (*d)[3];
	  *dlen -= (*d)[0] + 4;
	  *d = (int *)((int)*d + (*d)[0] + 4);
	  return;
	}
      *dlen -= (*d)[0] + 4;
      *d = (int *)((int)*d + (*d)[0] + 4);
    }
}

static unsigned int
memorycopy (unsigned int copyto, void *copyfrom, int copylen)
{
  unsigned char *p, *q;

  p = (void *)copyto;
  q = copyfrom;
  while (copylen--)
    *p++ = *q++;
  return (unsigned int)p;
}

static unsigned int
copy_swapaddr (int first, unsigned int addr_tmp)
{
  unsigned int tmp[3];
  int *d1, d1len, *d2, d2len;
  unsigned int d1_addr, d1_len, d2_addr, d2_len;

  d1 = mmap, d1len = mmaplen;
  d2 = mmap, d2len = mmaplen;
  d1_len = 0;
  d2_len = 0;

  getnext (&d1, &d1len, &d1_addr, &d1_len);
  getnext (&d2, &d2len, &d2_addr, &d2_len);
  while (d2_addr < halfaddr)
    {
      if (d2_addr + d2_len > halfaddr)
	{
	  d2_len -= halfaddr - d2_addr;
	  d2_addr = halfaddr;
	  break;
	}
      getnext (&d2, &d2len, &d2_addr, &d2_len);
    }

  for (;;)
    {
      if (d1_len == 0)
	getnext (&d1, &d1len, &d1_addr, &d1_len);
      if (d2_len == 0)
	getnext (&d2, &d2len, &d2_addr, &d2_len);
      if (d1_addr >= (halfaddr - 8192 + 4096))
	break;
      if (d1_addr + d1_len > (halfaddr - 8192 + 4096))
	d1_len = (halfaddr - 8192 + 4096) - d1_addr;
      if (d1_len == 0)
	{
	  print ("d1_len == 0\n");
	  return 0;
	}
      if (d2_len == 0)
	{
	  print ("d2_len == 0\n");
	  return 0;
	}
      tmp[0] = d1_addr;
      tmp[1] = d2_addr;
      if (d1_len < d2_len)
	tmp[2] = d1_len;
      else
	tmp[2] = d2_len; 
      addr_tmp = memorycopy (addr_tmp, tmp, sizeof tmp);
      d1_addr += tmp[2];
      d2_addr += tmp[2];
      d1_len -= tmp[2];
      d2_len -= tmp[2];
    }
  tmp[0] = first;
  tmp[1] = 0;
  tmp[2] = 0;
  addr_tmp = memorycopy (addr_tmp, tmp, sizeof tmp);
  return addr_tmp;
}

static unsigned int
copy_firstaddr (unsigned int addr_tmp, unsigned int addr_first, unsigned int l)
{
  unsigned int d1_addr, d1_len, d2_addr, d2_len, totallen;
  unsigned int tmp[3];
  int *d2, d2len;
  int v;

  d2 = mmap, d2len = mmaplen;
  d1_addr = 0;
  d1_len = 0;
  d2_len = 0;
  totallen = 0;

  d2_addr = addr_first;
  d2_len = l;

  for (;;)
    {
      if (totallen >= (all / 2 - 8192 + 4096))
	break;
      if (d1_len == 0)
	{
	  d1_addr = verifyinfo[v].addr;
	  d1_len = verifyinfo[v].len;
	  v++;
	}
      if (d2_len == 0)
	{
	  getnext (&d2, &d2len, &d2_addr, &d2_len);
	  while (d2_addr < halfaddr)
	    {
	      if (d2_addr + d2_len > halfaddr)
		{
		  d2_len -= halfaddr - d2_addr;
		  d2_addr = halfaddr;
		  break;
		}
	      getnext (&d2, &d2len, &d2_addr, &d2_len);
	    }
	}
      if (d1_len == 0)
	{
	  print ("verify d1_len == 0\n");
	  return 0;
	}
      if (d2_len == 0)
	{
	  print ("verify d2_len == 0\n");
	  return 0;
	}
      if (d1_len <= 0x20)
	continue;
      tmp[0] = d2_addr;
      tmp[1] = d1_addr;
      if (d1_len - 0x20 < d2_len)
	tmp[2] = d1_len - 0x20;
      else
	tmp[2] = d2_len;
      if (totallen + tmp[2] > (all / 2 - 8192 + 4096))
	tmp[2] = (all / 2 - 8192 + 4096) - totallen;
      addr_tmp = memorycopy (addr_tmp, tmp, sizeof tmp);
      d1_addr += tmp[2] + 0x20;
      d2_addr += tmp[2];
      d1_len -= tmp[2] + 0x20;
      d2_len -= tmp[2];
      totallen += tmp[2];
    }
  tmp[0] = 0;
  tmp[1] = 0;
  tmp[2] = 0;
  addr_tmp = memorycopy (addr_tmp, tmp, sizeof tmp);
  return addr_tmp;
}

void
prepare_boot (int first)
{
  unsigned int addr_wkp, addr_tmp, addr_realfacs, addr_fakefacs, addr_first;
  unsigned int tmp[2];
  char firstdata[FIRSTDATASIZE];
  int i;

  if (first)
    verify ();

  splitaddr ();
  if (alllen < 4096)
    {
      print ("last available space < 4096 bytes\n");
      return;
    }

  /* OS should use: size = all / 2 - 8192, end addr = halfaddr - 8192 */
  /* fake FACS: addr = halfaddr - 8192 */
  /* swapping size: all / 2 - 8192 + 4096 (includes fake FACS) */
  /* real FACS: addr = alladdr - 4096 */
  /* code: addr = alladdr - 2048 */

  addr_realfacs = get_facs_addr ();
  *(int *)(addr_realfacs + 12) = 0x220;
  addr_fakefacs = halfaddr - 8192;
  memorycopy (addr_fakefacs, (void *)addr_realfacs, 1024);
  modify_facp (halfaddr - 8192);

  addr_wkp = alladdr - 2048;

  /* fill addresses in the code */
  wkrb1 |= addr_wkp & 0xffffff;
  wkrb2 |= addr_wkp >> 24;
  wkpbse = addr_wkp;
  wkpsp = addr_wkp + (wkpend - wkp);

  /* first */
  if (first)
    {
      for (i = 0; i < sizeof firstdata; i++)
	firstdata[i] = 0;
      for (i = 0; i < sizeof firstdata; i++)
	if (!(firstdata[i] = HEADERTEXT[i]))
	  break;
      wkpsp += sizeof firstdata;
    }

  /* copy a real address mode code */
  memorycopy (0x220, wkr, wkrend - wkr);

  /* copy a protected mode code */
  addr_tmp = memorycopy (addr_wkp, wkp, wkpend - wkp);
  addr_first = addr_tmp;
  if (first)
    addr_tmp = memorycopy (addr_tmp, firstdata, sizeof firstdata);

  addr_tmp = wkpsp;
  tmp[0] = addr_wkp + (wkp32 - wkp);
  tmp[1] = 0x18;
  addr_tmp = memorycopy (addr_tmp, tmp, sizeof tmp);

  /* copy swapping addresses */
  addr_tmp = copy_swapaddr (first, addr_tmp);

  if (first)
    addr_tmp = copy_firstaddr (addr_tmp, addr_first, sizeof firstdata);

  if (!first)
    {
      tmp[0] = addr_realfacs;
      tmp[1] = addr_fakefacs;
      addr_tmp = memorycopy (addr_tmp, tmp, sizeof tmp);
    }
}

void
prepare_boot_1stmode (void)
{
  unsigned int addr_wkp, addr_tmp, addr_realfacs, addr_fakefacs;
  unsigned int tmp[2], tmp3[3];
  int i;

  splitaddr ();
  if (alllen < 4096)
    {
      print ("last available space < 4096 bytes\n");
      return;
    }

  /* OS should use: size = all / 2 - 8192, end addr = halfaddr - 8192 */
  /* fake FACS: addr = halfaddr - 8192 */
  /* swapping size: all / 2 - 8192 + 4096 (includes fake FACS) */
  /* real FACS: addr = alladdr - 4096 */
  /* code: addr = alladdr - 2048 */

  addr_realfacs = get_facs_addr ();
  *(int *)(addr_realfacs + 12) = 0x220;
  addr_fakefacs = halfaddr - 8192;
  memorycopy (addr_fakefacs, (void *)addr_realfacs, 1024);
  modify_facp (halfaddr - 8192);

  addr_wkp = alladdr - 2048;

  /* fill addresses in the code */
  wkrb1 |= addr_wkp & 0xffffff;
  wkrb2 |= addr_wkp >> 24;
  wkpbse = addr_wkp;
  wkpsp = addr_wkp + (wkpend - wkp);

  /* copy a real address mode code */
  memorycopy (0x220, wkr, wkrend - wkr);

  /* copy a protected mode code */
  addr_tmp = memorycopy (addr_wkp, wkp, wkpend - wkp);

  addr_tmp = wkpsp;
  tmp[0] = addr_wkp + (wkp32 - wkp);
  tmp[1] = 0x18;
  addr_tmp = memorycopy (addr_tmp, tmp, sizeof tmp);

  /* copy addresses */
  tmp3[0] = 2;
  tmp3[1] = 0;
  tmp3[2] = 0;
  addr_tmp = memorycopy (addr_tmp, tmp3, sizeof tmp3);
  addr_tmp = copy_swapaddr (0, addr_tmp);

  tmp[0] = addr_realfacs;
  tmp[1] = addr_fakefacs;
  addr_tmp = memorycopy (addr_tmp, tmp, sizeof tmp);
}

void
prepare_boot_2ndmode (void)
{
  unsigned int addr_wkp, addr_tmp, addr_realfacs, addr_fakefacs;
  unsigned int tmp[2];
  int i;

  splitaddr ();
  if (alllen < 4096)
    {
      print ("last available space < 4096 bytes\n");
      return;
    }

  /* OS should use: size = all / 2 - 8192, end addr = halfaddr - 8192 */
  /* fake FACS: addr = halfaddr - 8192 */
  /* swapping size: all / 2 - 8192 + 4096 (includes fake FACS) */
  /* real FACS: addr = alladdr - 4096 */
  /* code: addr = alladdr - 2048 */

  addr_realfacs = get_facs_addr ();
  *(int *)(addr_realfacs + 12) = 0x220;
  addr_fakefacs = halfaddr - 8192;
  memorycopy (addr_fakefacs, (void *)addr_realfacs, 1024);
  modify_facp (halfaddr - 8192);

  addr_wkp = alladdr - 2048;

  /* fill addresses in the code */
  wkrb1 |= addr_wkp & 0xffffff;
  wkrb2 |= addr_wkp >> 24;
  wkpbse = addr_wkp;
  wkpsp = addr_wkp + (wkpend - wkp);

  /* copy a real address mode code */
  memorycopy (0x220, wkr, wkrend - wkr);

  /* copy a protected mode code */
  addr_tmp = memorycopy (addr_wkp, wkp, wkpend - wkp);

  addr_tmp = wkpsp;
  tmp[0] = addr_wkp + (wkp32 - wkp);
  tmp[1] = 0x18;
  addr_tmp = memorycopy (addr_tmp, tmp, sizeof tmp);

  /* copy swapping addresses */
  addr_tmp = copy_swapaddr (0, addr_tmp);

  tmp[0] = addr_realfacs;
  tmp[1] = addr_fakefacs;
  addr_tmp = memorycopy (addr_tmp, tmp, sizeof tmp);
}

static unsigned int
find_header (unsigned a1, unsigned a0, unsigned b1, unsigned b0)
{
  unsigned char *p;
  unsigned *q, sum;
  int i;

  asm ("add %3, %1; adc %2, %0" : "+m" (b1), "+m" (b0) : "r" (a1), "r" (a0));
  if (a1)
    {
      print ("area above 4GB ignored\n");
      return;
    }
  if (b1)
    {
      print ("end address truncated to 4GB\n");
      b0 = ~0;
    }
  if (a0 < 0x110000)
    a0 = 0x110000;
  for (a0 = (a0 + BLK - 1) & ~(BLK - 1); a0 + (BLK - 1) <= b0; a0 += BLK)
    {
      if (!(a0 & 0xfffff))
	print ("\r%x", a0);
      p = (void *)a0;
      for (i = 0; i < FIRSTDATASIZE; i++)
	{
	  if (p[i] != HEADERTEXT[i])
	    break;
	  if (HEADERTEXT[i] == '\0')
	    goto f;
	}
      continue;
    f:
      print ("signature at %x\n", a0);
      for (; i < FIRSTDATASIZE; i++)
	if (p[i] != '\0')
	  break;
      if (i < FIRSTDATASIZE)
	continue;
      print ("signature at %x\n", a0);
      q = (void *)p;
      for (sum = 0, i = 0; i < (FIRSTDATASIZE + 0x20) / 4; i++)
	sum += q[i];
      print ("sum %x\n", sum);
      if (sum == 0)
	return a0;
    }
  return 0;
}

void
second_boot (void)
{
  int *d = mmap, len = mmaplen;
  unsigned int addr_header = 0, *p, copylen, i, sum, num;
  unsigned char *copyfrom, *copyto;

  while (len >= 4)
    {
      if (d[5] == 1)
	{
	  print ("size %x addr %x:%x length %x:%x type %x\n",
		 d[0], d[2], d[1], d[4], d[3], d[5]);
	  addr_header = find_header (d[2], d[1], d[4], d[3]);
	  if (addr_header)
	    break;
	}
      len -= d[0] + 4;
      d = (int *)((int)d + d[0] + 4);
    }
  copylen = FIRSTDATASIZE;
  copyfrom = (void *)addr_header;
  copyto = 0;
  for (num = 0; copylen; num++)
    {
      p = (void *)copyfrom;
      for (sum = 0, i = 0; i < copylen + 0x20; i += 4)
	sum += *p++;
      print ("from  %x  to    %x  len   %x  sum   %x  ",
	     copyfrom, copyto, copylen, sum);
      p = (void *)(copyfrom + copylen);
      copyfrom = (void *)p[1];
      copyto = (void *)p[0];
      copylen = p[2];
    }
  while (--num)
    {
      copylen = FIRSTDATASIZE;
      copyfrom = (void *)addr_header;
      for (i = 0; i < num; i++)
	{
	  p = (void *)(copyfrom + copylen);
	  copyfrom = (void *)p[1];
	  copyto = (void *)p[0];
	  copylen = p[2];
	}
      print ("%x->%x(%x)  ", copyfrom, copyto, copylen);
      i = copylen;
      while (i--)
	copyto[i] = copyfrom[i];
    }
  print (" done\n");
}
