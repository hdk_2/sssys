extern char cmdline[1024];
extern unsigned char bootdrive;

void
printinfo (void)
{
  print ("cmdline \"%s\"\n", cmdline);
  print ("bootdrive %x\n", bootdrive);
  dumpmap ();
  splitaddr ();
}

void
menu1 (void)
{
  int c;

  for (;;)
    {
      print ("1. Fill memory with test pattern\n");
      print ("2. Reboot\n");
      print ("3. Verify test pattern\n");
      print ("4. Boot\n");
      print ("5. First boot\n");
      print ("6. Second boot\n");
      print ("9. Print information\n");
      print ("? ");
      c = getch ();
      print ("%c\n", c);
      switch (c)
	{
	case '1':
	  filltest ();
	  break;
	case '2':
	  reboot ();
	  break;
	case '3':
	  verify ();
	  break;
	case '4':
	  boot ();
	  break;
	case '5':
	  prepare_boot (1);
	  break;
	case '6':
	  second_boot ();
	  prepare_boot (0);
	  break;
	case '9':
	  printinfo ();
	  break;
	default:
	  print ("?\n");
	}
    }
}

void
boot_1stmode (void)
{
  prepare_boot_1stmode ();
  boot ();
}

void
boot_2ndmode (void)
{
  int i;
  unsigned long long lba;
  static unsigned char buff[512];
  unsigned int startaddr, endaddr, addr;

  for (i = 0; i < sizeof cmdline; i++)
    {
      if (cmdline[i + 0] == '\0' || cmdline[i + 1] == '\0' ||
	  cmdline[i + 2] == '\0' || cmdline[i + 3] == '\0')
	break;
      if (cmdline[i + 0] == 'l' && cmdline[i + 1] == 'b' &&
	  cmdline[i + 2] == 'a' && cmdline[i + 3] == '=')
	goto found;
    }
  print ("please specify lba\n");
  return;
 found:
  lba = 0;
  for (i += 4; i < sizeof cmdline && cmdline[i]; i++)
    {
      if (cmdline[i] >= '0' && cmdline[i] <= '9')
	lba = (lba << 3) + (lba << 1) + (cmdline[i] & 0xf);
      else
	break;
    }
  if (readex (buff, &lba))
    {
      print ("read failed\n");
      return;
    }
  if (buff[0] != 's' || buff[1] != 's' || buff[2] != 's' || buff[3] != 'y' ||
      buff[4] != 's' || buff[5] != '\0')
    {
      print ("keyword mismatch\n");
      return;
    }
  startaddr = *(int*)&buff[0x100];
  endaddr = *(int*)&buff[0x104];
  lba++;
  for (addr = startaddr; addr < endaddr; addr += 512, lba++)
    {
      if (!((addr - startaddr) & 1048575))
	print ("\r%x", endaddr - addr);
      if (readex ((void *)addr, &lba))
	{
	  print ("read (addr=%x, lba=%x:%x) failed\n", addr,
		 ((int *)&lba)[1], ((int *)&lba)[0]);
	  return;
	}
    }
  print ("\nok\n");
  prepare_boot_2ndmode ();
  boot ();
}

void
menu2 (void)
{
  int c;

  for (;;)
    {
      print ("1. First boot\n");
      print ("2. Second boot\n");
      print ("9. Print information\n");
      print ("0. Reboot\n");
      print ("? ");
      c = getch ();
      print ("%c\n", c);
      switch (c)
	{
	case '1':
	  boot_1stmode ();
	  break;
	case '2':
	  boot_2ndmode ();
	  break;
	case '9':
	  printinfo ();
	  break;
	case '0':
	  reboot ();
	  break;
	default:
	  print ("?\n");
	}
    }
}
