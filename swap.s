	.code16
	wakeadr = 0x220
	gdtoff  = 0x10
	gdtroff = 0x12
	.global	wkr
	.global	wkrb1
	.global	wkrb2
	.global	wkrend
wkr:	cli
	lgdt	%cs:gdtroff
	mov	%cr0, %eax
	inc	%ax
	mov	%eax, %cr0
	ljmp	$8, $0
	.org	wkr + gdtroff + 1
	.byte	0xff
	.long	wakeadr + gdtoff
	.short	0xffff
wkrb1:	.short	0x0000			# base field is physical address of wkp
	.byte	0x00			#
	.byte	0x9b
	.byte	0x8f
wkrb2:	.byte	0x00
	.org	wkr + 0x20
wkrend:

	.global	wkp
	.global	wkpbse
	.global	wkpsp
	.global	wkp32
	.global	wkpend
wkp:	jmp	1f
	.org	wkp + 0x2
wkpgdt:	.short	0xffff
wkpbse:	.long	0			# physical address of wkpgdt-2 = wkp
	.quad	0x008f93000000ffff
	.quad	0x00cf93000000ffff
	.quad	0x00cf9b000000ffff
wkpsp:	.space	4			# physical address of stack
1:	lgdtl	%cs:wkpgdt - wkp
	mov	$0x10, %ax		# 32bit data segment
	mov	%ax, %ds
	mov	%ax, %ss
	mov	%cs:wkpsp - wkp, %esp
	lretl
wkp16:	mov	%cr0, %eax
	dec	%ax
	mov	%eax, %cr0
	jmp	1f
1:	mov	%ebx, %cs:wkpjmp - wkp
	jmp	1f
1:	.byte	0xea
wkpjmp:	.long	0
	.code32
wkp32:	jmp	3f
1:	mov	(%esi, %ecx), %eax
	mov	(%edi, %ecx), %edx
	mov	%eax, (%edi, %ecx)
	mov	%edx, (%esi, %ecx)
2:	sub	$4, %ecx
	jnb	1b
3:	pop	%esi
	pop	%edi
	pop	%ecx
	test	%ecx, %ecx
	jne	2b
	test	%esi, %esi
	jne	wkp1st
	pop	%eax			# real FACS address
	movl	$wakeadr, 12(%eax)	# update waking vector
	pop	%eax			# fake FACS address
	mov	12(%eax), %ebx		# load waking vector
	shl	$12, %ebx
	shr	$12, %bx
jmpebx:	mov	$8, %ax			# 16bit data segment
	mov	%eax, %ds
	mov	%eax, %ss
	lgdt	wakeadr+0x12
	ljmp	$8, $wkp16 - wkp
wkp1st:	cmp	$2, %esi
	je	wkp1stmode
	pop	%esi
	pop	%edi
	pop	%ecx
3:	lea	(%edi, %ecx), %edx
	xor	%ebx, %ebx
	jmp	2f
1:	mov	(%esi), %eax
	mov	%eax, (%edi)
	add	$4, %esi
	add	$4, %edi
	add	%eax, %ebx
2:	sub	$4, %ecx
	jnb	1b
	pop	%esi
	pop	%edi
	pop	%ecx
	mov	%esi, 0(%edx)
	add	%esi, %ebx
	mov	%edi, 4(%edx)
	add	%edi, %ebx
	mov	%ecx, 8(%edx)
	add	%ecx, %ebx
	neg	%ebx
	mov	%ebx, 12(%edx)
	xor	%eax, %eax
	mov	%eax, 16(%edx)
	mov	%eax, 20(%edx)
	mov	%eax, 24(%edx)
	mov	%eax, 28(%edx)
	test	%ecx, %ecx
	jne	3b
	movw	$0x1234, 0x472		# warm reboot
	mov	$0xffff0000, %ebx
	jmp	jmpebx

############################################################
# 1st mode: just copy lower memory to higher then continue

1:	mov	(%esi, %ecx), %eax
	mov	%eax, (%edi, %ecx)
2:	sub	$4, %ecx
	jnb	1b
wkp1stmode:
	pop	%esi
	pop	%edi
	pop	%ecx
	test	%ecx, %ecx
	jne	2b
	pop	%eax			# real FACS address
	movl	$wakeadr, 12(%eax)	# update waking vector
	pop	%eax			# fake FACS address
	mov	12(%eax), %ebx		# load waking vector
	shl	$12, %ebx
	shr	$12, %bx
	jmp	jmpebx
	
	.align	4
wkpend:
	# stack
	# +0  physical address of wkp32
	# +4  0x18 (selector)
	# +8  %esi
	# +12 %edi
	# +16 %ecx
	# +20 repeat
	# +nn 1
	# +nn 0
	# +nn 0
	# +nn %esi
	# +nn %edi
	# +nn %ecx
	# +nn repeat
	# +nn 0
	# +nn 0
	# +nn 0
	# +nn physical address of real FACS
	# +nn physical address of fake FACS
