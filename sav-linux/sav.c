#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <asm/io.h>
#include <asm/uaccess.h>

#define SAVEDEVICE "/dev/hda6"

static int sav_pm_suspend (struct device *dev, pm_message_t pm_state);
static int sav_pm_resume (struct device *dev);
static struct platform_device *pdev;
static struct file *fp;

static struct device_driver sav_drv = {
  .name = "sav",
  .bus = &platform_bus_type,
  .owner = THIS_MODULE,
  .suspend = sav_pm_suspend,
  .resume = sav_pm_resume,
};

static void
readtest (void)
{
  char buf[512];
  loff_t off;
  int i;
  mm_segment_t oldfs = get_fs ();

  off = 0;
  set_fs (get_ds ());
  printk ("read %d:", vfs_read (fp, buf, 512, &off));
  set_fs (oldfs);
  for (i = 0; i < 512; i++)
    {
      if (buf[i] >= ' ' && buf[i] <= '~')
	printk ("%c", buf[i]);
      else
	printk (".");
    }
  printk ("\n");
}

static void
readmemtest (void)
{
  char __iomem *p;
  unsigned char c;
  int i;

  p = ioremap (0, 0x1000);
  for (i = 0; i < 64; i++)
    {
      c = readb (p + i);
      printk ("%02x ", c);
    }
  printk ("\n");
  iounmap (p);
  p = ioremap (0x80000000, 0x1000);
  for (i = 0; i < 64; i++)
    {
      c = readb (p + i);
      printk ("%02x ", c);
    }
  printk ("\n");
  iounmap (p);
}

static unsigned int
sssys_getwkp (void)
{
  unsigned char __iomem *p;
  unsigned int wkpaddr = 0;

  p = ioremap (0x220, 0x20);
  if (!p)
    return 0;
  if (readb (p + 0x18) == 0xff && readb (p + 0x19) == 0xff)
    {
      wkpaddr = readb (p + 0x1f);
      wkpaddr = (wkpaddr << 8) | readb (p + 0x1c);
      wkpaddr = (wkpaddr << 8) | readb (p + 0x1b);
      wkpaddr = (wkpaddr << 8) | readb (p + 0x1a);
    }
  iounmap (p);
  if (!wkpaddr)
    return 0;
  p = ioremap (wkpaddr, 0x8);
  if (!p)
    return 0;
  if (readb (p + 2) != 0xff || readb (p + 3) != 0xff ||
      wkpaddr != readl (p + 4))
    wkpaddr = 0;
  iounmap (p);
  return wkpaddr;
}

static int
readsub (struct file *filep, void *buf, int len, loff_t *off)
{
  int r;
  mm_segment_t oldfs = get_fs ();

  set_fs (get_ds ());
  r = vfs_read (filep, buf, len, off);
  set_fs (oldfs);
  return r;
}

static int
writesub (struct file *filep, void *buf, int len, loff_t *off)
{
  int r;
  mm_segment_t oldfs = get_fs ();

  set_fs (get_ds ());
  r = vfs_write (filep, buf, len, off);
  set_fs (oldfs);
  return r;
}

static void
savedata (void)
{
  loff_t off;
  static char savebuff[512];
  unsigned int wkp, wkpstk, savestart, addr, i;
  unsigned char __iomem *p;

  /* read the device file. check it is correct or not */
  off = 0;
  if (readsub (fp, savebuff, 512, &off) != 512)
    {
      printk ("savedata: read error\n");
      return;
    }
  if (strcmp (savebuff, "sssys"))
    {
      printk ("savedata: keyword mismatch\n");
      return;
    }
  /* correct. */

  /* check wkp address */
  wkp = sssys_getwkp ();
  if (wkp == 0)
    {
      printk ("bad wkp\n");
      return;
    }

  /* load wkpstk */
  p = ioremap (wkp, 0x24);
  if (!p)
    {
      printk ("ioremap failed\n");
      return;
    }
  wkpstk = readl (p + 0x20);
  iounmap (p);

  /* check stack data */
  p = ioremap (wkpstk, 8192);
  if (!p)
    {
      printk ("ioremap stack failed\n");
      return;
    }
  if (readl (p + 4) != 0x18)
    {
      printk ("stack data error\n");
      iounmap (p);
      return;
    }
  if (readl (p + 8) != 2)
    {
      printk ("second boot?\n");
      iounmap (p);
      return;
    }
  savestart = readl (p + 0x18);
  iounmap (p);

  /* save data */
  *(unsigned int *)&savebuff[0x100] = savestart;
  *(unsigned int *)&savebuff[0x104] = wkp;
  off = 0;
  if (writesub (fp, savebuff, 512, &off) != 512)
    {
      printk ("savedata: write error 0\n");
      return;
    }
  for (addr = savestart; addr < wkp; addr += 512)
    {
      p = ioremap (addr, 512);
      if (!p)
	{
	  printk ("ioremap save failed\n");
	  return;
	}
      for (i = 0; i < 512; i += 4)
	*(unsigned int *)&savebuff[i] = readl (p + i);
      iounmap (p);
      if (writesub (fp, savebuff, 512, &off) != 512)
	{
	  printk ("savedata: write error 1\n");
	  return;
	}
      if (!(off & 1048575))
	{
	  printk ("\r%x ", (unsigned int)off);
	}
    }
  do_sync_file_range (fp, 0, off - 1, SYNC_FILE_RANGE_WAIT_BEFORE |
		      SYNC_FILE_RANGE_WRITE | SYNC_FILE_RANGE_WAIT_AFTER);
  filp_close (fp, NULL);
  printk ("done\n");
  kernel_restart (NULL);
}

static int
sav_pm_suspend (struct device *dev, pm_message_t pm_state)
{
  printk ("sav suspend\n");
  return 0;
}

static int
sav_pm_resume (struct device *dev)
{
  printk ("sav resume\n");
  savedata ();
  //readtest ();
  return 0;
}

static int __init
sav_init (void)
{
  int r;
  unsigned int wkp;

  printk ("sav load\n");
  wkp = sssys_getwkp ();
  printk ("wkp = 0x%x\n", wkp);
  if (wkp == 0)
    return -EINVAL;
  r = driver_register (&sav_drv);
  if (r < 0)
    return r;
  pdev = platform_device_register_simple ("sav", -1, NULL, 0);
  if (IS_ERR (pdev))
    {
      driver_unregister (&sav_drv);
      return PTR_ERR (pdev);
    }
  fp = filp_open (SAVEDEVICE, O_RDWR | O_LARGEFILE, 0);
  if (IS_ERR (fp))
    {
      platform_device_unregister (pdev);
      driver_unregister (&sav_drv);
      printk ("filp_open failed\n");
      return PTR_ERR (fp);
    }
  //readtest ();
  //readmemtest ();
  printk ("sav load ok\n");
  return 0;
}

static void __exit
sav_exit (void)
{
  printk ("sav unload\n");
  //readtest ();
  filp_close (fp, NULL);
  platform_device_unregister (pdev);
  driver_unregister (&sav_drv);
  printk ("sav unload ok\n");
}

module_init (sav_init);
module_exit (sav_exit);
MODULE_LICENSE("GPL");
