	.text
	.global	readex
readex:	push	%ebp
	mov	%esp, %ebp
	push	%ebx
	push	%esi
	push	%edi
	movl	$0x00010010, 0x7800
	movl	$0x00007c00, 0x7804
	mov	12(%ebp), %ebx
	mov	0(%ebx), %eax
	mov	%eax, 0x7808
	mov	4(%ebx), %eax
	mov	%eax, 0x780c
	mov	bootdrive, %dl
	mov	$0x7800, %esi
	mov	$0x42, %ah
	int	$0x13
	jc	err
	mov	$0x7c00, %esi
	mov	8(%ebp), %edi
	mov	$512 / 4, %ecx
	cld
	rep	movsl
	clc
err:	sbb	%eax, %eax
	pop	%edi
	pop	%esi
	pop	%ebx
	pop	%ebp
	ret
